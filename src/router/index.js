import { createRouter, createWebHistory } from "vue-router";
import HomeComponent from "../components/HomeComponent.vue";
import HelpComponent from "../components/HelpComponent.vue";
import ProfileComponent from "../components/ProfileComponent.vue";
import AccountComponent from "../components/AccountComponent.vue";
import LoginView from "../views/LoginView.vue";
import ReadView from "../views/ReadView.vue";
import ForgotPasswordView from "../views/ForgotPasswordView.vue";
import ResetPasswordView from "../views/ResetPasswordView.vue";
import LinksComponent from "../components/LinksComponent.vue";
import FavoritesComponent from "../components/FavoritesComponent.vue";
import ArchiveComponent from "../components/ArchiveComponent.vue";
import HighlightsComponent from "../components/HighlightsComponent.vue";
import TagsComponent from "../components/TagsComponent.vue";
import NotFoundComponent from "../components/error/NotFoundComponent.vue";
import { useAuth } from "../store/auth";

const routes = [
  {
    path: "/",
    name: "main",
    component: HomeComponent,
  },
  {
    path: "/help",
    name: "home",
    component: HelpComponent,
  },
  {
    path: "/profile",
    name: "profile",
    component: ProfileComponent,
    meta: { requiresAuth: true },
    children: [
      {
        path: ":id",
        meta: { requiresAuth: false },
        component: ProfileComponent,
      },
    ],
  },
  {
    path: "/account",
    name: "account",
    component: AccountComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/forgot",
    name: "forgot",
    component: ForgotPasswordView,
  },
  {
    path: "/reset-password",
    name: "reset-password",
    component: ResetPasswordView,
  },
  {
    path: "/links",
    name: "links",
    component: LinksComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/favorites",
    name: "favorites",
    component: FavoritesComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/archive",
    name: "archive",
    component: ArchiveComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/highlights",
    name: "highlights",
    component: HighlightsComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/tags",
    name: "tags",
    component: TagsComponent,
    meta: { requiresAuth: true },
  },
  {
    path: "/read/:id",
    name: "read",
    component: ReadView,
    meta: { requiresAuth: true },
  },
  {
    path: "/:catchAll(.*)",
    name: "notFound",
    component: NotFoundComponent,
    meta: {
      requiresAuth: false,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  const auth = useAuth();
  if (to.meta.requiresAuth && !auth.status.loggedIn) {
    return {
      path: "/",
    };
  }
});

export default router;
