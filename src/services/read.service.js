import axiosInstance from "@/interceptor/axiosInterceptor";

const API_URL = "http://localhost:8083/api/read";

class ReadService {
  getSourceCodeById(id) {
    return axiosInstance.get(`${API_URL}`, { params: { id: id } });
  }

  getSourceCodeByUrl(url) {
    return axiosInstance.get(`${API_URL}`, {
      params: { url: encodeURIComponent(url) },
    });
  }
}

export default new ReadService();
