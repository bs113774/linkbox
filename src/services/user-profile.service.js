import axiosInstance from "@/interceptor/axiosInterceptor";

const API_URL = "http://localhost:8083/api/profile";

class UserProfileService {
  getUserProfile() {
    return axiosInstance.get(API_URL).then((response) => {
      return response.data;
    });
  }

  getUserProfileById(id) {
    return axiosInstance.get(`${API_URL}/${id}`);
  }

  async changeUserProfileId(request) {
    return axiosInstance.post(API_URL, {
      oldId: request.oldId,
      newId: request.newId,
      password: request.password,
    });
  }
}

export default new UserProfileService();
