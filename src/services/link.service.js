import axiosInstance from "@/interceptor/axiosInterceptor";

const API_URL = "http://localhost:8083/api/links";

class LinkService {
  getLinks(linkType, page, sort) {
    return axiosInstance
      .get(`${API_URL}`, {
        params: { linkType: linkType, page: page, sort: sort },
      })
      .then((response) => {
        return response?.data;
      });
  }

  getLink(id) {
    return axiosInstance.get(`${API_URL}/${id}`).then((response) => {
      return response?.data;
    });
  }

  addLink(url, title, description) {
    return axiosInstance.post(
      API_URL,
      { title: title, description: description },
      {
        params: { url: encodeURIComponent(url) },
      }
    );
  }

  patchLink(id, toggleParam, tags) {
    return axiosInstance.patch(API_URL + "/" + id, tags, {
      params: { toggle: toggleParam },
      headers: { "Content-Type": tags ? "application/json" : "" },
    });
  }

  deleteLink(id) {
    return axiosInstance.delete(`${API_URL}/${id}`);
  }

  shareLink(id) {
    return axiosInstance.post(`${API_URL}/${id}/share`);
  }

  deleteSharedLink(id) {
    return axiosInstance.delete(`${API_URL}/${id}/share`);
  }
}

export default new LinkService();
