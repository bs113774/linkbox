import axios from "axios";
import axiosInstance from "@/interceptor/axiosInterceptor";
import router from "@/router";

const API_URL = "http://localhost:8083/api/auth/";

class AuthService {
  async login(user) {
    return axios
      .post(API_URL + "login", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.data.token) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
    router.go(0);
  }

  async register(user) {
    return axios
      .post(API_URL + "register", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        return response.data;
      });
  }

  async changePassword(request) {
    return axiosInstance.post(API_URL + "change-password", {
      oldPassword: request.oldPassword,
      newPassword: request.newPassword,
    });
  }

  async changeEmail(request) {
    return axiosInstance.post(API_URL + "change-email", {
      newEmail: request.newEmail,
      password: request.password,
    });
  }
}

export default new AuthService();
