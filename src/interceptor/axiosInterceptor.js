import axios from "axios";
import { useAuth } from "@/store/auth";
import router from "@/router";
import { AuthHeader } from "@/services/auth.header";

const axiosInstance = axios.create({
  headers: {
    Authorization: AuthHeader.authHeader(),
  },
});

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const auth = useAuth();
    if (error.response.status === 400) {
      return error.response;
    }
    if (error.response.status === 401) {
      auth.logout();
      router.push({ name: "main" });
    }
    if (error.response.status === 404) {
      router.push({ name: "main" });
    }
    if (error.response.status === 500) {
      return error.response;
    }
  }
);

export default axiosInstance;
